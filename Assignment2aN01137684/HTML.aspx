﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTML.aspx.cs" Inherits="Assignment2aN01137684.HTML" %>

<asp:Content ID="CodeExample" ContentPlaceHolderID="MainContent" runat="server">
    <h2>HTML</h2>
    <div class="row">
        <h3>HTML Form</h3>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <h4>Example from class</h4>
            
            <usercontrol:codeBox ID="teacher_html_code" runat="server"
                SkinId="codedataGrid" code="HTML" owner="Teacher">
             </usercontrol:codeBox>
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
            <p>Code Explaination</p>
            <p>In this example form used for customer information,Form allow user to enter their data.</p>
            <p>Here user required their first name and last name to submit form. </p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Links" ContentPlaceHolderID="Links" runat="server">
    <h2>Useful Links for HTML</h2>
    <ul>
        <li><a href="https://www.w3schools.com/html/">HTML W3C</a></li>
        <li><a href="https://websitesetup.org/html-tutorial-beginners/">HTML Tutorial</a></li>
        <li><a href="https://www.tutorialspoint.com/html/">Tutorials Point</a></li>
        <li><a href="https://www.w3schools.com/html/html5_intro.asp">HTML 5</a></li>
        <li><a href="https://www.quora.com/What-is-the-latest-version-of-HTML">HTML version</a></li>
    </ul>
</asp:Content>
<asp:Content ID="Code" ContentPlaceHolderID="ExCode" runat="server">
    <div class="row">
        <h3>HTML address:</h3>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <h4>Example from tutorial</h4>
            
            <usercontrol:codeBox ID="my_html_code" runat="server"
                SkinId="codedataGrid" code="HTML" owner="Me">
             </usercontrol:codeBox>
            
        </div>
        <div class="col-md-6 col-lg-8 col-xs-12">
            <p>Code Explaination</p>
            <p>The HTML address element defines contact information (author/owner) of a document or article.</p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="CodeExplain" ContentPlaceHolderID="logoOfContent" runat="server">
    <img class="img-thumbnail" src="images/html.png" alt="DB" />
</asp:Content>
