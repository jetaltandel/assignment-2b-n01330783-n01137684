﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2aN01137684.usercontrol
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string code
        {
            get { return (string)ViewState["code"]; }
            set { ViewState["code"] = value; }
        }
        public string owner
        {
            get { return (string)ViewState["owner"]; }
            set { ViewState["owner"] = value; }
        }

        DataView CreateCode(List<string> codeList)
        {
            DataTable databaseCode = new DataTable();
            DataColumn ln_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            ln_col.ColumnName = "Line";
            ln_col.DataType = System.Type.GetType("System.Int32");
            databaseCode.Columns.Add(ln_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            databaseCode.Columns.Add(code_col);
            

            int i = 0;
            foreach (string code_line in codeList)
            {
                coderow = databaseCode.NewRow();
                coderow[ln_col.ColumnName] = i;
                string final_code = System.Net.WebUtility.HtmlEncode(code_line);
                final_code = final_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = final_code;

                i++;
                databaseCode.Rows.Add(coderow);
            }

            DataView codeview = new DataView(databaseCode);
            return codeview;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            

            List<string> sql_teacher_code = new List<string>(new string[]{
                "CREATE TABLE books (",
                "~book_id NUMBER(7) PRIMARY KEY,",
                "~book_title VARCHAR2(25),",
                "~book_author VARCHAR2(20) NOT NULL,",
                "~book_type VARCHAR(15)",
                ")"
            });
            List<string> sql_my_code = new List<string>(new string[]{
                "SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate",
                "~FROM Orders",
                "~INNER JOIN Customers",
                "~ON Orders.CustomerID=Customers.CustomerID;"
            });
            List<string> js_teacher_code = new List<string>(new string[]{
                "function avg(num1,num2,num3,num4,num5)",
                "~~{,",
                "~~ar avg = 0;",
                "~~avg = (num1+num2+num3+num4+num5)/5;",
                "}",
                "var grade = avg(20,30,40,22,45);"
            });
            List<string> js_my_code = new List<string>(new string[]{
                "var myObject = {",
                "firstName:Ekta,",
                "lastName: Patel,",
                "fullName: function() {",
                "return this.firstName + this.lastName;",
                "}"
            });
                      
            List<string> html_teacher_code = new List<string>(new string[]{
             "<h1> Customer Information:<h1>",
             "~<form action = #  method= post >",
             "~~<div>",
             "~~~<lable  for=fname>First Name: </lable>",
             "~~~<input  type = text id=fname name = firstname />",
             "~~</div>",
             "~~<div>",
             "~~~<lable for=lname> Last Name:</lable>",
             "~~~<input type=text  id =lname  name=lastname>",
             "~~</div>",
             "~~<div>",
             "~~<button type=submit  id=Submit  name=submitbutton> Submit </button>",
             "~~</div>",
             "</form>"
            });
            List<string> html_my_code = new List<string>(new string[]{
             "<!DOCTYPE html>",
             "~<html>",
             "~<body>",
             "~<address>",
             "~~Written by John Doe.<br>",
             "~~Visit us at:< br >",
             "~~Example.com < br >",
             "~~Box 564, Disneyland<br>",
             "~~USA",
             "~~</ address >",
             "~  </ body >",
             "~</ html >"
            });
            
            List<string> final_code = new List<string>(new String[] { });
            if (code == "JS" && owner == "Me")
            {
                final_code = js_my_code;
            }
            else if (code == "JS" && owner == "Teacher")
            {
                final_code = js_teacher_code;
            }
            else if (code == "Database" && owner == "Me")
            {
                final_code = sql_teacher_code;
            }
            else if (code == "Database" && owner == "Teacher")
            {
                final_code = sql_my_code;
            }
            else if (code == "HTML" && owner == "Me")
            {
                final_code = html_my_code;
            }
            else if (code == "HTML" && owner == "Teacher")
            {
                final_code = html_teacher_code;
            }

            DataView dv = CreateCode(final_code);
            code_container.DataSource = dv;
            code_container.DataBind();

        }
    }
}